# importing libraries 
import os
import graphene
from flask import Flask
from flask_graphql import GraphQLView
from flask_sqlalchemy import SQLAlchemy
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField

app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))

# Database Configs
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'database.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

# Initialize Database
db = SQLAlchemy(app)


# ------------------  Database Models ------------------

class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    brand = db.Column(db.String(256))
    size = db.Column(db.String(256))
    features = db.Column(db.Text)
    quantity = db.Column(db.Integer)
    price = db.Column(db.String(256)) 
   

    def __repr__(self):
        return '<Product %r>' % self.name


# ------------------ Graphql Schemas ------------------


# Objects Schema
class ProductObject(SQLAlchemyObjectType):
    class Meta:
        model = Product
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    all_products = SQLAlchemyConnectionField(ProductObject)


schema_query = graphene.Schema(query=Query)


# Flask Rest & Graphql Routes
@app.route('/')
def hello_world():
    return 'Hello From Graphql Tutorial!'


# /graphql-query
app.add_url_rule('/graphql-query', view_func=GraphQLView.as_view(
    'graphql-query',
    schema=schema_query, graphiql=True
))


if __name__ == '__main__':
    app.run()
