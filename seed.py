# This is to feed data to the database

from app import db, Product

db.create_all()     # create tables from models


product1 = Product()
product1.name = "Laptop"
product1.brand = "Acer"
product1.size = "15 inch"
product1.features = "Full HD IPS Display, 8th Generation Intel Core i5-8265U, NVIDIA GeForce MX250"
product1.quantity = 5
product1.price = "Rs. 80000"

db.session.add(product1)
db.session.commit()

print(Product.query.all())
