### GraphQL with Graphene in Python Flask
This a a very basic app to demonstrate a simple implementation of graphene in python flask.

### Features
- The app generates the information of product stored in database (sqlite)

### Requirements
- Flask
- Graphene
- Flask-graphql
- Flask-migrate
- Flask-SQLAlchemy
- Graphene SQLAlchemy
- Postman 

### Instructions to run the app
- Clone the repo in your local project folder ``` git clone https://gitlab.com/gayatribudha/grapheneinflask.git```
- Install all the required libraries running the command inside your main project directory ```pip install requirements.txt```
- Run the command ```python app.py``` in the terminal staying in the same directory
- Copy the provided url

### Testing the API in Postman
- Open the postman
- Set the request to GET and paste the url (http://127.0.0.1:5000/graphql-query) 
- Set the value of content-type to appplication/graphql and in body write following query,

```
    query{
    	allProducts{
    		edges{
    			node{
    				name
    				brand
    				size
    				features
    				quantity
    				price
    			}
    		}
    	}
    }
 ```
- Hit the send button